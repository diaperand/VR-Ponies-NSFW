﻿namespace VRTK.Examples
{
	using UnityEngine;

	public class Vibrator : VRTK_InteractableObject
	{
		// If it's being held, don't vibrate
		public float shakeAmount = 0.15f;

		private Vector3 originalPos;
		private Quaternion originalRot;
		private bool isBeingHeld = false;
		private VRTK_ControllerReference controllerReference;

		// Use this for initialization
		protected void Start () {
			originalPos = transform.position;
			originalRot = transform.rotation;
		}

		public override void Grabbed(GameObject grabbingObject)
		{
			base.Grabbed (grabbingObject);
			isBeingHeld = true;

			controllerReference = VRTK_ControllerReference.GetControllerReference(grabbingObject);
		}

		public override void Ungrabbed(GameObject grabbingObject)
		{
			base.Ungrabbed (grabbingObject);
			isBeingHeld = false;

			originalPos = transform.position;
			originalRot = transform.rotation;
		}
			
		protected override void Update()
		{
			base.Update();

			if (!isBeingHeld) {
				transform.position = originalPos + Random.insideUnitSphere * shakeAmount / 100f;
				transform.rotation = originalRot;
			} else {
				// Vibrate controller
				VRTK_ControllerHaptics.TriggerHapticPulse(controllerReference, shakeAmount * 100f, 0.01f, 0.01f);
			}
		}
	}
}