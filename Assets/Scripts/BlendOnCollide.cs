﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlendOnCollide : MonoBehaviour
{
	public List<int> blendIndexes;
	public float minBlend = 0f;
	public float maxBlend = 100f;
	public float speed = 3f;
	public SkinnedMeshRenderer skinnedMeshRenderer;

	private float blend = 0f;
	private bool isColliding = false;

	void Awake ()
	{
		// If SMR not set, get our own
		if (!skinnedMeshRenderer) {
			skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer> ();
		}
	}

	void Start ()
	{

	}

	void ChangeBlend(float delta) {
		blend+=delta;

		// Stay within our bounds
		if (blend > maxBlend)
			blend = maxBlend;
		else if (blend < minBlend)
			blend = minBlend;
		
		foreach (var blendIndex in blendIndexes) {
			skinnedMeshRenderer.SetBlendShapeWeight (blendIndex, blend);
		}
	}

	void Update ()
	{
		// Increase or decrease blend depending on if we're colliding
		if (isColliding)
			ChangeBlend (speed);
		else
			ChangeBlend (-speed);
	}

	// Trigger activation and deactivation
	void OnTriggerEnter (Collider otherObject) {
		Debug.Log ("Close is colliding!");
		if (otherObject.gameObject.tag == "Toy")
			isColliding = true;
	}

	void OnTriggerExit (Collider otherObject) {
		if (otherObject.gameObject.tag == "Toy")
			isColliding = false;
	}
}