﻿/* Only enable scripts when they fall within a SnapDropZone */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableInSnapDropZone : VRTK.VRTK_InteractableObject {

	public MonoBehaviour[] scripts;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	protected override void Update () {
		base.Update ();

		foreach (MonoBehaviour script in scripts)
			if (IsInSnapDropZone())
				script.enabled = true;
			else
				script.enabled = false;
	}
}
