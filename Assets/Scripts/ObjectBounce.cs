﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBounce : MonoBehaviour {

	public float bounceHeight = 1f;
	public float bounceSpeed = 1f;

	private bool bobbingUp = true;
	private float moveHeight;
	private float count;

	// Use this for initialization
	void Awake () {
		moveHeight = bounceHeight / 10000f;
		count = Time.time;
	}

	// Update is called once per frame
	void Update () {
		// Move up when we need to be, otherwise move down
		if (bobbingUp)
			transform.Translate (moveHeight * Vector3.up);
		else
			transform.Translate (moveHeight * -Vector3.up);

		// Switch between moving up and down every bounceSpeed seconds
		if (Time.time - count > 1 / bounceSpeed) {
			count = Time.time;
			bobbingUp = !bobbingUp;
		}
	}
}