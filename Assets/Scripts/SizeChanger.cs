﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeChanger : MonoBehaviour {
	public GameObject partTrigger;
	public GameObject partGrip;
	private SteamVR_TrackedObject trackedObject;
	private SteamVR_Controller.Device device;

	// Use this for initialization
	void Start () {
		trackedObject = GetComponentInParent<SteamVR_TrackedObject> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (trackedObject != null) {
			device = SteamVR_Controller.Input ((int)trackedObject.index);
			if (device.GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
				Debug.Log ("Trigger Press");
				device.TriggerHapticPulse (700);
				partTrigger.transform.localScale = new Vector3 (partTrigger.transform.localScale.x + 0.005f, partTrigger.transform.localScale.y + 0.005f, partTrigger.transform.localScale.z + 0.005f);
			}
			if (device.GetPress (SteamVR_Controller.ButtonMask.Grip)) {
				Debug.Log ("Grip Press");
				device.TriggerHapticPulse (700);
				partGrip.transform.localScale = new Vector3 (partGrip.transform.localScale.x + 0.005f, partGrip.transform.localScale.y + 0.005f, partGrip.transform.localScale.z + 0.005f);
			}
		}
	}
}
