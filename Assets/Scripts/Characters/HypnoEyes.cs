﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Spin eyes around slightly to give a 'hypnosis-like' effect
 */

public class HypnoEyes : MonoBehaviour {
	public Material leftEyeMaterial;
	public Material rightEyeMaterial;

	public float spinRadius = 1.0f;
	public float spinSpeed = 1.0f;
	public float eyeSplitter = 1.0f;

	public bool horizontalSpin = false;
	public bool offsetEyes = false;

	private float offsetX;
	private float offsetY;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!horizontalSpin) {
			// Lower spinRadius impact by a factor of 100
			float radius = spinRadius / 10f;

			// Offset texture location
			leftEyeMaterial.mainTextureOffset = new Vector2 (Mathf.Sin (Time.time * spinSpeed) * radius + -eyeSplitter, Mathf.Cos (Time.time * spinSpeed) * radius);
			rightEyeMaterial.mainTextureOffset = new Vector2 (Mathf.Sin (Time.time * spinSpeed + (offsetEyes ? 10f : 0.0f)) * -radius + eyeSplitter, Mathf.Cos (Time.time * spinSpeed + (offsetEyes ? 10f : 0.0f)) * radius);
		} else {
			// Offset texture location only in the horizontal direction
			leftEyeMaterial.mainTextureOffset = new Vector2 (leftEyeMaterial.mainTextureOffset.x + spinSpeed / 100f, leftEyeMaterial.mainTextureOffset.y);
			rightEyeMaterial.mainTextureOffset = new Vector2 (rightEyeMaterial.mainTextureOffset.x + spinSpeed / 100f, rightEyeMaterial.mainTextureOffset.y);
		}
	}
}
