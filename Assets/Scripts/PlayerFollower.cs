﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour {

	public Transform target;
	public float moveSpeed = 3f;
	public float rotationSpeed = 3f;
	public float farRange = 10f;
	public float nearRange = 10f;
	public float stopRange = 0f;

	private Transform myTransform;
	private Animator anim;

	private int runHash;
	private int sitHash;

	// Use this for initialization
	void Start () {
		myTransform = transform; // Cache initial transform
		anim = GetComponent<Animator>();

		sitHash = Animator.StringToHash ("Sitting");
		runHash = Animator.StringToHash ("Running");
	}
	
	// Update is called once per frame
	void Update () {
		if (target) {
			Vector3 targetDistNormalized = new Vector3 (target.position.x, myTransform.position.y, target.position.z);
			float distance = Vector3.Distance (myTransform.position, targetDistNormalized);

			if (distance > stopRange && distance <= nearRange) {
				// Move towards the player
				anim.SetBool("Running", true);
				myTransform.rotation = Quaternion.Slerp (myTransform.rotation,
					Quaternion.LookRotation (target.position - myTransform.position), rotationSpeed * Time.deltaTime);

				// Only move in X/Z plane
				Vector3 newPosition = myTransform.position + myTransform.forward * moveSpeed * Time.deltaTime;
				newPosition.y = myTransform.position.y;
				myTransform.position = newPosition;
			} else {
				// Stop running
				anim.SetBool("Running", false);
			}

			// Rotate to face player
			Quaternion newRotation = Quaternion.Slerp (myTransform.rotation,
				Quaternion.LookRotation (target.position - myTransform.position), rotationSpeed * Time.deltaTime);
			
			if (distance <= farRange) {
				// Don't rotate if we've stopped and are already sort of facing the target
				if (distance <= stopRange && newRotation.eulerAngles.y < 90) {
					return;
				}

				// Only rotate on Y axis
				myTransform.eulerAngles = new Vector3 (0, newRotation.eulerAngles.y, 0);
			} else {
				// Stop running
				anim.SetBool("Running", false);
			}
				
		}
	}
}
