﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlendPendulum : MonoBehaviour
{
	public List<int> blendIndexes;
	public float minBlend = 0f;
	public float maxBlend = 100f;
	public float speed = 3f;
	public bool useCollider = false;
	public SkinnedMeshRenderer skinnedMeshRenderer;

	private Mesh skinnedMesh;
	private float blend = 0f;
	private bool isIncreasing = true;
	private bool isColliding = false;

	void Awake ()
	{
		// If SMR not set, get our own
		if (!skinnedMeshRenderer) {
			skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer> ();
		}
	}

	void Start ()
	{

	}

	void ChangeBlend(float delta) {
		blend+=delta;
		foreach (var blendIndex in blendIndexes) {
			skinnedMeshRenderer.SetBlendShapeWeight (blendIndex, blend);
		}
	}

	void Update ()
	{
		// Don't animate if the user has set an activation collider trigger
		// and it's not colliding with something!
		if (useCollider && !isColliding) {
			ChangeBlend (-blend);
			return;
		}

		// Switch between 100% and 0% over and over
		if (isIncreasing) {
			ChangeBlend (speed);
		} else {
			ChangeBlend (-1f);
		}

		if (!isIncreasing && blend < minBlend) {
			blend = minBlend;
			isIncreasing = true;
		} else if (isIncreasing && blend > maxBlend) {
			isIncreasing = false;
		}
	}

	// Trigger activation and deactivation
	void OnTriggerEnter (Collider otherObject) {
		Debug.Log ("In Mouth is colliding!");
		if (otherObject.gameObject.tag == "Toy")
			isColliding = true;
	}

	void OnTriggerExit (Collider otherObject) {
		if (otherObject.gameObject.tag == "Toy")
			isColliding = false;
	}
}