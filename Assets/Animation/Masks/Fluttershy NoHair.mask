%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Fluttershy NoHair
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: 0_Skeleton
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/LeftShoulder
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/LeftShoulder/LeftForearm
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/LeftShoulder/LeftForearm/LeftHand
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/LeftShoulder/LeftForearm/LeftHand/LeftBall
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/LeftShoulder/LeftForearm/LeftHand/LeftBall/LeftBall_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/HairBack1
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/HairBack1/jiggle_hair3
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/HairBack1/jiggle_hair3/jiggle_hair6
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/HairBack1/jiggle_hair3/jiggle_hair6/jiggle_hair7
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/HairBack1/jiggle_hair3/jiggle_hair6/jiggle_hair7/jiggle_hair7_end
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/LeftLip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/LeftLip/LeftLip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/LeftLipMiddle
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/LeftLipMiddle/LeftLipMiddle_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/RightLip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/RightLip/RightLip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/RightLipMiddle
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/RightLipMiddle/RightLipMiddle_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/TongueRoot
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/TongueRoot/TongueMid
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/TongueRoot/TongueMid/TongueTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Javv/TongueRoot/TongueMid/TongueTip/TongueTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/jiggle_hair1
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/jiggle_hair1/jiggle_hair2
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/jiggle_hair1/jiggle_hair2/jiggle_hair4
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/jiggle_hair1/jiggle_hair2/jiggle_hair4/jiggle_hair5
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/jiggle_hair1/jiggle_hair2/jiggle_hair4/jiggle_hair5/jiggle_hair5_end
    m_Weight: 0
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftCheek
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftCheek/LeftCheek_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftCornerLip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftCornerLip/LeftCornerLip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEar
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEar/LeftEarTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEar/LeftEarTip/LeftEarTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeBrow
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeBrow/LeftEyeBrowBackTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeBrow/LeftEyeBrowBackTip/LeftEyeBrowBackTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeBrow/LeftEyeBrowFrontTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeBrow/LeftEyeBrowFrontTip/LeftEyeBrowFrontTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeLidLowerer
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeLidLowerer/LeftEyeLidLowerer_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeLidUpper
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftEyeLidUpper/LeftEyeLidUpper_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftLipUp
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftLipUp/LeftLipUp_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftLipUpMiddle
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/LeftLipUpMiddle/LeftLipUpMiddle_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Nose
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/Nose/Nose_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightCheek
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightCheek/RightCheek_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightCornerLip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightCornerLip/RightCornerLip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEar
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEar/RightEarTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEar/RightEarTip/RightEarTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeBrow
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeBrow/RightEyeBrowBackTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeBrow/RightEyeBrowBackTip/RightEyeBrowBackTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeBrow/RightEyeBrowFrontTip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeBrow/RightEyeBrowFrontTip/RightEyeBrowFrontTip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeLidLowerer
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeLidLowerer/RightEyeLidLowerer_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeLidUpper
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightEyeLidUpper/RightEyeLidUpper_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightLipUp
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightLipUp/RightLipUp_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightLipUpMiddle
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/Neck/Head/RightLipUpMiddle/RightLipUpMiddle_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/RightShoulder
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/RightShoulder/RightForearm
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/RightShoulder/RightForearm/RightHand
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/RightShoulder/RightForearm/RightHand/RightBall
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/Chest2/RightShoulder/RightForearm/RightHand/RightBall/RightBall_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingClosed
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingClosed/LeftWingClosed1
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingClosed/LeftWingClosed1/LeftWingClosed2
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingClosed/LeftWingClosed1/LeftWingClosed2/LeftWingClosed2_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftDownOpen01
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftDownOpen01/LeftDownOpen01_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftDownOpen02
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftDownOpen02/LeftDownOpen02_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftDownOpen03
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftDownOpen03/LeftDownOpen03_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen01
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen01/LeftWingOpen01Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen01/LeftWingOpen01Tip/LeftWingOpen01Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen02
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen02/LeftWingOpen02Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen02/LeftWingOpen02Tip/LeftWingOpen02Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen03
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen03/LeftWingOpen03Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen03/LeftWingOpen03Tip/LeftWingOpen03Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen04
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen04/LeftWingOpen04Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/LeftWingOpen/LeftWingOpen0/LeftWingOpen04/LeftWingOpen04Tip/LeftWingOpen04Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingClosed
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingClosed/RightWingClosed1
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingClosed/RightWingClosed1/RightWingClosed2
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingClosed/RightWingClosed1/RightWingClosed2/RightWingClosed2_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightDownOpen01
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightDownOpen01/RightDownOpen01_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightDownOpen02
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightDownOpen02/RightDownOpen02_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightDownOpen03
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightDownOpen03/RightDownOpen03_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen01
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen01/RightWingOpen01Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen01/RightWingOpen01Tip/RightWingOpen01Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen02
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen02/RightWingOpen02Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen02/RightWingOpen02Tip/RightWingOpen02Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen03
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen03/RightWingOpen03Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen03/RightWingOpen03Tip/RightWingOpen03Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen04
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen04/RightWingOpen04Tip
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Chest1/RightWingOpen/RightWingOpen0/RightWingOpen04/RightWingOpen04Tip/RightWingOpen04Tip_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh/LeftButt
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh/LeftButt/LeftButt_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh/LeftLeg1
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh/LeftLeg1/LeftLeg2
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh/LeftLeg1/LeftLeg2/LeftFoot
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/LeftThigh/LeftLeg1/LeftLeg2/LeftFoot/LeftFoot_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh/RightButt
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh/RightButt/RightButt_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh/RightLeg1
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh/RightLeg1/RightLeg2
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh/RightLeg1/RightLeg2/RightFoot
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/RightThigh/RightLeg1/RightLeg2/RightFoot/RightFoot_end
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1/Tail2
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1/Tail2/Tail3
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1/Tail2/Tail3/Tail4
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1/Tail2/Tail3/Tail4/Tail5
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1/Tail2/Tail3/Tail4/Tail5/Tail6
    m_Weight: 1
  - m_Path: 0_Skeleton/Pelvis/Tail1/Tail2/Tail3/Tail4/Tail5/Tail6/Tail6_end
    m_Weight: 1
  - m_Path: body
    m_Weight: 1
  - m_Path: goggled_neck
    m_Weight: 1
  - m_Path: goggles_face
    m_Weight: 1
  - m_Path: mane
    m_Weight: 1
  - m_Path: physics
    m_Weight: 1
  - m_Path: tail
    m_Weight: 1
  - m_Path: wing_closed_l
    m_Weight: 1
  - m_Path: wing_closed_r
    m_Weight: 1
  - m_Path: wing_open_l
    m_Weight: 1
  - m_Path: wing_open_r
    m_Weight: 1
